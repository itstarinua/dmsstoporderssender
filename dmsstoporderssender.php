<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Dmsstoporderssender extends Module
{
    protected $config_form = false;
    public static $module_dir = __DIR__;

    public function __construct()
    {
        $this->name = 'dmsstoporderssender';
        $this->tab = 'emailing';
        $this->version = '0.0.1';
        $this->author = 'tomsky.pl';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Stop Orders Sender');
        $this->description = $this->l('Add function to the order for  stop sending e-mail  when changed status');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);   
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('DMSSTOPORDERSSENDER_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayAdminOrderLeft') &&
            $this->registerHook('actionDmsStopOrderSender');
    }

    public function uninstall()
    {
        Configuration::deleteByName('DMSSTOPORDERSSENDER_LIVE_MODE');

        return parent::uninstall();
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
//        if (Tools::getValue('module_name') == $this->name) {
//            $this->context->controller->addJS($this->_path.'views/js/back.js');
//            $this->context->controller->addCSS($this->_path.'views/css/back.css');
//        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
//        $this->context->controller->addJS($this->_path.'/views/js/front.js');
//        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookActionDmsStopOrderSender( $id_order = 0 )
    {
        $return = true;
        if( (int)$id_order )
        {
            $return = (bool)self::getSenderStatus( (int)$id_order );
        }
        return $return;
    }

    public function hookDisplayAdminOrderLeft( $params )
    {
        $set_status = 0;
        if( Tools::isSubmit('submitEmailSenderStatus') ) {            
            if( Tools::getValue('send_status_emails') == 'true' ){
                $set_status = 1;
            }
            self::setSenderStatus( $params['id_order'], $set_status );
        }
        $set_status = self::getSenderStatus( $params['id_order'] );
        $params['smarty']->assign('send_status_emails', $set_status);
        return $params['smarty']->fetch( self::$module_dir.'/views/templates/hook/adminOrder.tpl');
    }
    public static function getSenderStatus( $id_order = 0 )
    {
        $query = 'SELECT `StatusSender` FROM `'._DB_PREFIX_.'orders` WHERE id_order='.$id_order; 
        return Db::getInstance()->getValue($query, false);        
    }
    public static function setSenderStatus( $id_order = 0, $value = 0 )
    {
        if( (int)$id_order == 0) return false;
        $query = 'Update `'._DB_PREFIX_.'orders` SET `StatusSender`='.$value.' WHERE `id_order`='.$id_order;
        return Db::getInstance()->execute($query);
    }
}
