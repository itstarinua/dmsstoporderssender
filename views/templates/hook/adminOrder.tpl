<div id="formAddPaymentPanel" class="panel">
    <div class="panel-heading">
            <i class="icon-envelope-o"></i>
            {l s="Mailer status for sender"}
    </div>
    <form action="{$currentIndex|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id}&amp;token={$smarty.get.token}" method="post" class="form-horizontal well hidden-print">
            <div class="row">                    
                    <label class="col-lg-12">
                        {l s='Status of e-mail sender'}:
                    </label>
                    <div class="col-lg-9">
                        <p class="radio">
                            <label for="send_status_emails">
                                <input class="send_status_emails" id="send_status_emails" type="radio" name="send_status_emails" {if $send_status_emails}checked="checked"{/if} value="true">
                                {l s='SEND E-MAIL'}
                            </label>
                        </p>
                        <p class="radio">
                            <label for="dont_send_status_emails">
                                <input class="send_status_emails" id="dont_send_status_emails" type="radio" name="send_status_emails" {if !$send_status_emails}checked="checked"{/if} value="false">
                                {l s='BLOCK E-MAIL'}
                            </label>
                        </p>
                    </div>
                    <div class="col-lg-3">
                            <button type="submit" name="submitEmailSenderStatus" class="btn btn-primary">
                                    {l s='Update status'}
                            </button>
                    </div>
            </div>
    </form>
</div>