<?php

class OrderHistory extends OrderHistoryCore
{
    public function canSendEmail($id_order)
    {
        $query = 'SELECT `StatusSender` FROM `'._DB_PREFIX_.'orders` WHERE id_order='.$id_order; 
        $return = (int)Db::getInstance()->getValue($query, false);
        return (bool)$return;   
//        $return = true;
//       if ( Hook::getIdByName('actionDmsStopOrderSender') ) 
//       {
//           $return = (bool)Hook::exec('actionDmsStopOrderSender', $id_order);
//       }
//       return $return;       
    }
    public function sendEmail($order, $template_vars = false)
    {
        if( !$this->canSendEmail( (int)$this->id_order ) ) return true;
        return parent::sendEmail($order, $template_vars);
    }

}
